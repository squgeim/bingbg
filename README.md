bingbg
======

A simple Python program to download today's Bing wallpaper.

Requirements
------
You just need to have Python 2.7 in your system (Linux, Windows, Mac OS).

Working
------
1. Open congig.py in any text editor, set the resolution and destination.
2. Pop up the Terminal and do `python bingbg.py`. That is it. Just repeat this step once a day and you are all set.
